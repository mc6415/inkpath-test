import React from "react";

interface pageContext {
  page: number;
  pages: number;
  setPage: any;
}

let context: pageContext = {
  page: 0,
  pages: 0,
  setPage: null,
}

const PageContext = React.createContext(context);

export default PageContext;