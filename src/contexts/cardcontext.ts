import React from "react";

interface cardContext {
  archetypes: [{ value: string; label: string }] | null;
  archetype: any;
  setArchetype: any;
  cards: any;
  setCards: any;
  type: any;
  setType: any;
  dialogOpen: boolean,
  setDialogOpen: any,
  card: any,
  setCard: any,
  tab: number,
  setTab: any,
  name: any,
  setName: any,
}

let context: cardContext = {
  archetypes: null,
  archetype: "",
  setArchetype: null,
  cards: null,
  setCards: null,
  type: null,
  setType: null,
  dialogOpen: false,
  setDialogOpen: null,
  card: null,
  setCard: null,
  tab: 0,
  setTab: null,
  name: "",
  setName: null,
};

const CardContext = React.createContext(context);

export default CardContext;
