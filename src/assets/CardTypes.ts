const CardTypes: {
  value: string;
  label: string;
}[] = [
  { value: "Effect Monster", label: "Effect Monster" },
  { value: "Flip Effect Monster", label: "Flip Effect Monster" },
  { value: "Flip Tuner Effect Monster", label: "Flip Tuner Effect Monster" },
  { value: "Gemini Monster", label: "Gemini Monster" },
  { value: "Normal Monster", label: "Normal Monster" },
  { value: "Normal Tuner Monster", label: "Normal Tuner Monster" },
  { value: "Pendulum Effect Monster", label: "Pendulum Effect Monster" },
  {
    value: "Pendulum Flip Effect Monster",
    label: "Pendulum Flip Effect Monster",
  },
  { value: "Pendulum Normal Monster", label: "Pendulum Normal Monster" },
  {
    value: "Pendulum Tuner Effect Monster",
    label: "Pendulum Tuner Effect Monster",
  },
  { value: "Ritual Effect Monster", label: "Ritual Effect Monster" },
  { value: "Ritual Monster", label: "Ritual Monster" },
  { value: "Skill Card", label: "Skill Card" },
  { value: "Spell Card", label: "Spell Card" },
  { value: "Spirit Monster", label: "Spirit Monster" },
  { value: "Toon Monster", label: "Toon Monster" },
  { value: "Trap Card", label: "Trap Card" },
  { value: "Tuner Monster", label: "Tuner Monster" },
  { value: "Union Effect Monster", label: "Union Effect Monster" },
  { value: "Fusion Monster", label: "Fusion Monster" },
  { value: "Link Monster", label: "Link Monster" },
  {
    value: "Pendulum Effect Fusion Monster",
    label: "Pendulum Effect Fusion Monster",
  },
  { value: "Synchro Monster", label: "Synchro Monster" },
  {
    value: "Synchro Pendulum Effect Monster",
    label: "Synchro Pendulum Effect Monster",
  },
  { value: "Synchro Tuner Monster", label: "Synchro Tuner Monster" },
  { value: "XYZ Monster", label: "XYZ Monster" },
  {
    value: "XYZ Pendulum Effect Monster",
    label: "XYZ Pendulum Effect Monster",
  },
];

export default CardTypes;
