import CardList from "../components/CardList";
import Filters from "../components/Filters";

const Home = () => {

  return (
    <div>
      <Filters />
      <CardList />
    </div>
  );
};

export default Home;
