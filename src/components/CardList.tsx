import React, { useEffect, useContext, useState } from "react";
import CardContext from "../contexts/cardcontext";
import CardComponent from "./CardComponent";
import axios from "axios";
import Pagination from "./Pagination";
import PageContext from "../contexts/PageContext";

function CardList() {
  const { archetype, cards, setCards, type, name } = useContext(CardContext);
  let [page, setPage] = useState(0);

  const pageEntries = 12;

  function filteredCards(): any {
    if (name && name !== "") {
        let filteredCards =  cards.filter((x: any) => x.name.toLowerCase().indexOf(name.toLowerCase()) >= 0);
        console.log(filteredCards / pageEntries)
      return cards.filter(
        (x: any) => x.name.toLowerCase().indexOf(name.toLowerCase()) >= 0
      );
    }

    return cards;
  }

  const pages =
    filteredCards() && filteredCards().length > 0
      ? Math.floor(filteredCards().length / pageEntries)
      : 0;

  useEffect(() => {
    setPage(0);

    function query_url(): string {
      let query = "";

      if (archetype && archetype.value && archetype.value !== "All") {
        query += `archetype=${archetype.value}&`;
      }
      if (type) {
        query += `type=${type.value}&`;
      }

      return query;
    }

    let url = `https://db.ygoprodeck.com/api/v7/cardinfo.php?${query_url()}`;

    axios
      .get(url)
      .then((res) => {
        let { data } = res.data;

        setCards(data);
      })
      .catch((err) => {
        console.error("No Cards!");
        setCards(null);
      });
    return () => {
      // Cleanup stuff here
    };
  }, [archetype, setCards, type, name]);

  function cardComponents() {
    if (cards) {
      let filteredCards;

      if (name && name !== "") {
        filteredCards = cards.filter(
          (x: any) => x.name.toLowerCase().indexOf(name.toLowerCase()) >= 0
        );
      } else {
        filteredCards = cards;
      }
      let pageStart = page * pageEntries;
      let pageCards = filteredCards
        .slice(pageStart, pageStart + pageEntries)
        .map((x: any) => {
          return <CardComponent cardProp={x} key={x.name} />;
        });

      return pageCards;
    }

    return null;
  }

  return (
    <>
      <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 h-1/2">
        {cardComponents()}
        {/* {cardComponents()} */}
      </div>
      <div className="text-center">
        <PageContext.Provider value={{ page, pages, setPage }}>
          <Pagination />
        </PageContext.Provider>
      </div>
    </>
  );
}

export default CardList;
