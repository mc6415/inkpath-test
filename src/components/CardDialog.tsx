import { useContext } from "react";
import { Dialog } from "@headlessui/react";
import CardContext from "../contexts/cardcontext";
import CardInfo from "./CardInfo";
import TabList from "./TabList";
import SetInfo from "./SetInfo";

function CardDialog() {
  let { dialogOpen, setDialogOpen, card, tab } = useContext(CardContext);

  return (
    <Dialog
      open={dialogOpen}
      onClose={() => setDialogOpen(false)}
      className="fixed inset-0 z-10 overflow-y-auto"
    >
      <div className="flex items-center justify-center min-h-screen">
        <Dialog.Overlay className="fixed inset-0 bg-black opacity-80" />
        <div style={{ minHeight: '75vh'}} className="bg-white rounded w-10/12 mx-auto z-10">
          <div style={{ height: '75vh' }} className="flex p-4 flex-1">
            <div className="flex-1 hidden md:inline-flex">
              <img src={card.card_images[0].image_url} alt={card.name} className="object-contain" />
            </div>
            <div className="flex flex-col flex-auto p-8 w-0 h-full overflow-auto">
              <TabList />
              { tab === 1 && <CardInfo /> }
              { tab === 2 && <SetInfo /> }
            </div>
          </div>
          <button onClick={() => setDialogOpen(false)} className="w-full bg-blue-400 rounded-b h-10">
            {" "}
            Close{" "}
          </button>
        </div>
      </div>
    </Dialog>
  );
}

export default CardDialog;
