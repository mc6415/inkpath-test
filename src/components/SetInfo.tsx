import React, { useContext } from "react";
import CardContext from "../contexts/cardcontext";

export default function SetInfo() {
  let { card } = useContext(CardContext);

  let desktopTableRows = () => {
    let sets = card.card_sets;

    return sets.map((set: any, index: number) => (
      <tr className={index % 2 === 1 ? "bg-blue-400" : ""} key={set.set_code}>
        <td>{set.set_name}</td>
        <td>{set.set_code}</td>
        <td>{set.set_rarity}</td>
        <td>${set.set_price}</td>
      </tr>
    ));
  };

  let mobileTableRows = () => {
    let sets = card.card_sets;

    return sets.map((set: any, index: number) => (
      <tr key={set.set_code} className={index % 2 === 0 ? "bg-blue-400 whitespace-pre-wrap" : "whitespace-pre-wrap"}>
        {set.set_name + "\n"}
        {set.set_code + "\n"}
        {set.set_rarity + "\n"}
        {set.set_price + "\n"}
        {/* <span className={index % 2 === 1 ? "bg-blue-400" : ""}>
          {set.set_name}
        </span>
        <span className={index % 2 === 1 ? "bg-blue-400" : ""}>
          {set.set_code}
        </span>
        <span className={index % 2 === 1 ? "bg-blue-400" : ""}>
          {set.set_rarity}
        </span>
        <span className={index % 2 === 1 ? "bg-blue-400" : ""}>
          ${set.set_price}
        </span> */}
      </tr>
    ));
  }

  return (
    <>
      <div>
        <h1 className="font-bold text-4xl text-center">Set Info</h1>
      </div>
      <div className="overflow-auto text-center">
        {card.card_sets ? (
          <table className="table-auto w-full">
            <thead className="hidden md:table-header-group">
              <tr>
                <th>Set Name</th>
                <th>Set Code</th>
                <th>Rarity</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody className="hidden md:table-header-group">{desktopTableRows()}</tbody>
            <tbody className="table-header-group md:hidden">{mobileTableRows()}</tbody>
          </table>
        ) : (
          <h3 className="font-semibold text-xl">No set info</h3>
        )}
      </div>
    </>
  );
}
