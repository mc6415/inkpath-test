import { useContext } from "react";
import CardContext from "../contexts/cardcontext";

function CardComponent({ cardProp }: any) {
  let { setDialogOpen, setCard } = useContext(CardContext);

  function openDialog() {
    setCard(cardProp);
    setDialogOpen(true);
  }

  return (
    <div className="col-span-1 hover:bg-blue-200 cursor-pointer overflow-hidden">
        <img
          className="p-4 inline-block h-full w-full object-contain"
          src={cardProp.card_images[0].image_url}
          alt="card.name"
          onClick={() => openDialog()}
        />
    </div>
  );
}

export default CardComponent;
