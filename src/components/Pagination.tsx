import React, { useContext } from "react";
import PageContext from "../contexts/PageContext";

interface PageButtonProps {
  newPage: number;
}

function PageButton({ newPage }: PageButtonProps) {
  let { setPage, page } = useContext(PageContext);

  let buttonClasses = "p-5 m-2 border rounded-full border-white text-2xl inline"

  return (
    <button
      className={newPage === page ? `bg-white text-black ${buttonClasses}` : `text-white ${buttonClasses}`}
      onClick={() => setPage(newPage)}
    >
      {newPage + 1}
    </button>
  );
}

function Pagination() {
  let { page, pages } = useContext(PageContext);

  return (
    <div className="mt-4">
      {page !== 0 && <PageButton newPage={0} />}
      { page !== 0 && <span className="text-white font-bold text-5xl"> &larr; </span> }
      {page - 2 > 0 && <PageButton newPage={page - 2} />}
      {page - 1 > 0 && <PageButton newPage={page - 1} />}
      <PageButton newPage={page} />
      {page + 2 < pages && <PageButton newPage={page + 1} />}
      {page + 3 < pages && <PageButton newPage={page + 2} />}
      { page !== pages -1 && <span className="text-white font-bold text-5xl bottom-0">&rarr;</span> }
      {page !== pages - 1 && <PageButton newPage={pages - 1} />}
    </div>
  );
}

export default Pagination;
