import { useContext } from 'react';
import CardContext from '../contexts/cardcontext';

function PriceTable() {
  let { card } = useContext(CardContext);

  console.log(card.card_prices);

  function priceRows(): any {
    let prices = card.card_prices[0];

    let priceRows = [];

    for (const site in prices) {
      if (Object.prototype.hasOwnProperty.call(prices, site)) {
        const price = prices[site];
        
        priceRows.push(
          <tr key={`price-${site}`}>
            <td className="px-10 py-2">{site}</td>
            <td className="px-10">${price}</td>
          </tr>
        )
      }
    }

    return priceRows;
  }

  return(
    <table className="table-auto">
      <thead>
        <tr>
          <th>Site</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        { priceRows() }
      </tbody>
    </table>
  )
}

export default PriceTable;