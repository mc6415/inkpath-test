import { useContext } from "react";
import CardContext from "../contexts/cardcontext";
import PriceTable from "./PriceTable";

function prettifyDesc(desc: string): string {
  console.log(desc);

  let newDesc = desc.replace("●", "\n●");

  console.log(newDesc);

  return newDesc;
}

function CardInfo() {
  let { card } = useContext(CardContext);

  return (
    <>
      <div className="mb-4">
        <h1 className="font-bold text-4xl"> {card.name} </h1>
      </div>
      <div className="flex flex-row">
        <div className="flex-1">
          <h2 className="font-semibold text-xl w-1/4">Type:</h2>
          <span className="text-lg ml-4">{card.type}</span>
        </div>
        <div className="flex-1">
          <h2 className="font-semibold text-xl w-1/4">Archetype:</h2>
          <span className="text-lg ml-4">
            {card.archetype ? card.archetype : "None"}
          </span>
        </div>
      </div>
      <div className="flex flex-row">
        <div className="flex-1">
          <h2 className="font-semibold text-xl w-1/4">
            { card.type.toLowerCase() === 'xyz monster' ? 'Rank' : 'Level' }
          </h2>
          <span className="text-lg ml-4">
            { card.level }
          </span>
        </div>
      </div>
      {card.atk && (
        <div>
          <h2 className="font-semibold text-xl w-1/4">ATK / DEF</h2>
          <span className="text-lg ml-4">
            {card.atk} / {card.def}
          </span>
        </div>
      )}
      <div className="whitespace-pre-wrap mt-4">
        <p className="italic font-semibold">{prettifyDesc(card.desc)}</p>
      </div>
      <div
        className="my-10"
        style={{
          alignSelf: "center",
        }}
      >
        <PriceTable />
      </div>
    </>
  );
}

export default CardInfo;
