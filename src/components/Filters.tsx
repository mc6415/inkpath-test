import React, { useContext } from "react";
import Select from "react-select";
import CardContext from "../contexts/cardcontext";
import CardTypes from "../assets/CardTypes";
import _ from "lodash";

function Filters() {
  const { archetype, archetypes, setArchetype, setType, setName } = useContext(
    CardContext
  );

  const search =_.debounce((val) => {
    setName(val.target.value);
  }, 300);

  return (
    <div className="flex flex-col flex-auto px-8">
      <div className="flex flex-col md:flex-row flex-auto">
        <div className="flex-1 px-4">
          <label className="text-white">Archetype</label>
          <Select
            value={archetype}
            options={archetypes as any}
            onChange={(val) => setArchetype(val)}
            isClearable
          />
        </div>
        <div className="flex-1 px-4">
          <label className="text-white">Card Type</label>
          <Select
            options={CardTypes as any}
            onChange={(val) => setType(val)}
            isClearable
          />
        </div>
      </div>
      <div className="flex flex-col md:flex-col flex-auto mt-4">
        <div className="flex-1 px-4">
          <label className="text-white">
            Name
          </label>
          <input onChange={(val) => search(val)} className="w-full h-9 rounded p-2"></input>
        </div>
        <div className="flex-1 px-4"/>
      </div>
    </div>
  );
}

export default Filters;
