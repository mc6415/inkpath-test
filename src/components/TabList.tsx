import { useContext } from "react";
import CardContext from "../contexts/cardcontext";

function TabList() {
  let { setTab, tab } = useContext(CardContext);

  let activeClass =
    "flex-grow text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none text-blue-500 border-b-2 font-medium border-blue-500";
  let nonActiveClass =
    "flex-grow text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none";

  return (
    <div className="mb-4">
      <nav className="flex flex-col sm:flex-row w-full flex-grow">
        <button
          onClick={() => setTab(1)}
          className={ tab === 1 ? activeClass : nonActiveClass }
        >
          Card Details
        </button>
        <button
          onClick={() => setTab(2)}
          className={ tab === 2 ? activeClass : nonActiveClass }  
        >
          Sets
        </button>
      </nav>
    </div>
  );
}

export default TabList;
