import React, { useEffect, useState } from "react";
import axios from "axios";
import CardContext from "./contexts/cardcontext";
import Routes from "./router";
import CardDialog from "./components/CardDialog";
import "./App.css";

function App() {
  let [archetypes, setArchetypes] = useState<
    [{ value: string; label: string }]
  >([{ value: "", label: "" }]);

  let [archetype, setArchetype] = useState({ value: "", label: "" });
  let [cards, setCards] = useState([]);
  let [loaded, setLoaded] = useState(false);
  let [type, setType] = useState(null);
  let [dialogOpen, setDialogOpen] = useState(false);
  let [card, setCard] = useState();
  let [tab, setTab] = useState(1)
  let [ name, setName ] = useState("");

  useEffect(() => {
    axios.get("https://db.ygoprodeck.com/api/v7/archetypes.php").then((res) => {
      let archetypeList: [{ value: string; label: string }] = res.data.map(
        (x: any) => {
          return {
            value: x.archetype_name,
            label: x.archetype_name,
          };
        }
      );

      archetypeList.unshift({
        value: "All",
        label: "All",
      });

      setArchetypes(archetypeList);
      setLoaded(true);
    });
    return () => {
      // Cleanup stuff here
    };
  }, []);

  return (
    <CardContext.Provider
      value={{
        archetypes,
        archetype,
        setArchetype,
        cards,
        setCards,
        type,
        setType,
        dialogOpen,
        setDialogOpen,
        card,
        setCard,
        tab,
        setTab,
        name,
        setName
      }}
    >
      { card && <CardDialog /> }
      <div className="grid grid-cols-1 h-full">
        {loaded && <Routes />}
      </div>
    </CardContext.Provider>
  );
}

export default App;
